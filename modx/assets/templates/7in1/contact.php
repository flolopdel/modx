<?php
/**
 * Business_Success Contact Page
 * 
 */


/*---------------SET THE FOLLOWING CUSTOM VARIABLES-----------------*/

$enable_NA_format = true; // true or false to enable or disable North American phone number validation
$email_address_to = "andon.design@gmail.com"; // you can use a comma-separated format for multiple recipients.
$subject = "Contact Form submission from www.YourDomain.com";

/*----------------------END CUSTOM VARIABLES------------------------*/

$NA_phone_format = ( $enable_NA_format ) ? '_NA_format' : '';

//If the form is submitted
if( isset($_POST['submit']) ) {
    // Get form vars
    $contact_name = trim(stripslashes($_POST['contact_name']));
    $contact_email = trim($_POST['contact_email']);
    $contact_phone = trim($_POST["contact_phone{$NA_phone_format}"]);
    $contact_ext = trim($_POST["contact_ext{$NA_phone_format}"]);
    $contact_message = trim(stripslashes($_POST['contact_message']));

    // Error checking if JS is turned off
    if( $contact_name == '' ) { //Check to make sure that the name field is not empty
	$nameError = 'Please enter a name';
    } else if( strlen($contact_name) < 2 ) {
	$nameError = 'Your name must consist of at least 2 characters';
    }

    if( $contact_email == '' ) {
	$emailError = 'Please enter a valid email address';
    } else if( !valid_email( $contact_email ) ) {
	$emailError = 'Please enter a valid email address';
    }

    if( $NA_phone_format ) {
	if( !isPhoneNumberValid( $contact_phone ) || ( $contact_phone == '' && $contact_ext != '' ) ) {
	    $phoneError = 'phone number';
	}
	if( !eregi("^[0-9]{0,5}$", $contact_ext) ) { // check if the extension consists of 1 to 5 digits, or empty
	    $extError = 'extension';
	}
    }
    if( isset($phoneError) && isset($extError) ) {
	$phone_extError = "Please enter a valid $phoneError and $extError";
    } else if( isset($phoneError) ) {
	$phone_extError = "Please enter a valid $phoneError";
    } else if( isset($extError) ) {
	$phone_extError = "Please enter a valid $extError";
    }

    if( $contact_message == '' ) {
	$messageError = 'Please enter your message';
    }

    if( !isset($nameError) && !isset($emailError) && !isset($messageError) && !isset($phoneError) && !isset($extError)) {
	$ext = ( $contact_ext != '' ) ? ' ext.'.$contact_ext : '';
	$phone = ( $contact_phone != '' ) ? 'Phone: '.$contact_phone.$ext."\r\n" : '';
	// Send email
	$message_contents = "Sender's name: " . $contact_name . "\r\nE-mail: {$contact_email} \r\n{$phone}Message: " . $contact_message . " \r\n";
	$header = "From: $contact_name <".$contact_email.">\r\n";
	$header .= "Reply-To: $contact_email\r\n";
	$header .= "Return-Path: $contact_email\r\n";
	$emailSent = ( mail_utf8( $email_address_to, $subject, $message_contents, $header ) ) ? true : false;
	
	$contact_name_thx = $contact_name;

	// Clear the form
	$contact_name = $contact_email = $contact_phone = $contact_ext = $contact_message = '';
    }
}




?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" dir="ltr" lang="en-US">
	<head profile="http://gmpg.org/xfn/11">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Contact &laquo; Business Success</title>
		<!--[if IE 6]>
		<script src="scripts/DD_belatedPNG_0.0.8a-min.js"></script>
		<script>
		DD_belatedPNG.fix('.pngfix, img, #recentcomments li');
		</script>
		<![endif]-->
		<meta name="robots" content="index,follow" />
		<link rel="stylesheet" id="reset-css" href="styles/common-css/reset.css?ver=1.0" type="text/css" media="screen" />
		<link rel="stylesheet" id="text-css" href="styles/style1/css/text.css?ver=1.0" type="text/css" media="screen" />
		<link rel="stylesheet" id="grid-960-css" href="styles/common-css/960.css?ver=1.0" type="text/css" media="screen" />
		<link rel="stylesheet" id="superfish_menu-css" href="scripts/superfish-1.4.8/css/superfish.css?ver=1.0" type="text/css" media="screen" />
		<link rel='stylesheet' id='pagination-css'  href='styles/common-css/pagenavi-css.css?ver=1.0' type='text/css' media='screen' />
		<link rel="stylesheet" id="style-css" href="styles/style1/css/style.css?ver=1.0" type="text/css" media="screen" />
		<link rel="stylesheet" id="pretty_photo-css" href="scripts/prettyPhoto/css/prettyPhoto.css?ver=1.0" type="text/css" media="screen" />
		<script type="text/javascript" src="scripts/jquery-1.3.2.min.js?ver=2.9.2"></script>
		<script type="text/javascript" src="scripts/cycle/jquery.cycle.all.js"></script>
		<script type="text/javascript" src="scripts/cycle/jquery.easing.1.1.1.js"></script>
		<script type="text/javascript" src="scripts/cufon/cufon-yui.js?ver=1.09i"></script>
		<script type="text/javascript" src="scripts/cufon/eurofurence_500-eurofurence_700.font.js?ver=1.0"></script>
		<script type="text/javascript" src="scripts/prettyPhoto/js/jquery.prettyPhoto.js?ver=2.5.6"></script>
		<script type="text/javascript" src="scripts/jquery-validate/jquery.validate.min.js?ver=1.6"></script>
		<script type="text/javascript" src="scripts/masked-input-plugin/jquery.maskedinput.min.js?ver=1.2.2"></script>
		<script type="text/javascript" src="scripts/superfish-1.4.8/js/hoverIntent.js?ver=1.0.0"></script>
		<script type="text/javascript" src="scripts/superfish-1.4.8/js/superfish.js?ver=1.4.8"></script>
		<script type="text/javascript" src="scripts/superfish-1.4.8/js/supersubs.js?ver=0.2.0"></script>
		<script type="text/javascript" src="scripts/script.js?ver=1.0"></script>
		<!--[if lte IE 7]>
		<link rel="stylesheet" href="styles/common-css/ie6-7.css" media="screen"
		type="text/css" />
		<style type="text/css">
		body{ behavior: url("scripts/csshover3.htc"); }
		</style>
		<![endif]-->
		<!--[if IE 6]>
		<link rel="stylesheet" href="styles/common-css/ie6.css" media="screen"
		type="text/css" />
		<![endif]-->
	</head>
	<body>
		<div id="wrapper-1" class="pngfix">
			<div id="wrapper-2" class="pngfix">
				<div id="page" class="container_16">
					<div class="clear"></div>
					<div id="top" class="grid_16">
						<div id="logo" class="grid_9 alpha">
							<h1>
								<a class="pngfix" style="background: transparent url( styles/style1/images/logo.png ) no-repeat 0 100%;" href="index.html">Business Success</a>
							</h1>
						</div>
						<div id="slogan" class="grid_9">Slogan tag line goes here&#8230;</div>
						<!-- end logo slogan -->
						<div id="search" class="grid_5 alpha prefix_11">
							<form action="" method="get">
								<div class="search_box">
									<input id="search_field" name="s" type="text" class="inputbox_focus inputbox pngfix" value="Search..." />
									<input type="submit" value="" class="search-btn pngfix" />
								</div>
							</form>
						</div>
						<!-- end search -->
					</div>
					<!-- end top -->
					<div class="clear"></div>
					<div id="dropdown-holder" class="grid_16">
						<div class="nav_bg pngfix">
							<ul class="sf-menu">
								<li><a href="index.html"><span>Home</span></a></li>
								<li><a href="about.html"><span>About</span></a>
									<ul>
										<li><a href="#"><span>History</span></a></li>
										<li><a href="#"><span>Our Team</span></a></li>
										<li><a href="#"><span>Employment</span></a>
											<ul>
												<li><a href="#"><span>Internal Staffing</span></a></li>
												<li><a href="#"><span>Consulting</span></a></li>
												<li><a href="#"><span>Child Item</span></a></li>
												<li><a href="#"><span>Child Item</span></a></li>
											</ul>
										</li>
										<li><a href="#"><span>Our Company</span></a></li>
									</ul>
								</li>
								<li><a href="portfolio.html"><span>Portfolio</span></a></li>
								<li><a href="features.html"><span>Features</span></a>
									<ul>
										<li><a href="preset-styles.html"><span>Preset Styles</span></a></li>
										<li><a href="full-width.html"><span>Full-width Page</span></a></li>
										<li><a href="help-prettyPhoto.html"><span>Help with prettyPhoto</span></a></li>
									</ul>
								</li>
								<li><a href="typography.html"><span>Typography</span></a></li>
								<li><a href="blog.html"><span>Blog</span></a></li>
								<li class="current_page_item"><a href="contact.php"><span>Contact</span></a></li>
							</ul>
						</div>
					</div>
					<!-- end dropdown-holder -->
					<div class="clear"></div>
					<div id="page_container" class="grid_16">
						<div id="container_bkgnd_btm" class="grid_16 alpha omega pngfix">
							<div id="page_content" class="grid_11 push_4 lelfPadding20">
								<div class="post" id="post-29">
									<h2>Contact</h2>
									<div class="entry">
										<p>Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim
											justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu
											pede
											mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi.
											Aenean vulputate eleifend tellus.</p>
									</div>
								</div>
								<br />
								<div class="clear"></div>
								<div id="contactInfo">
									<div class="grid_2 contactFieldDesc">Address:</div>
									<div class="grid_8 contactFieldValue">123 Street Name, Suite #</div>
									<div class="clear"></div>
									<div class="grid_2 contactFieldDesc"></div>
									<div class="grid_8 contactFieldValue">City, State 12345, Country</div>
									<div class="clear"></div>
									<div class="grid_2 contactFieldDesc">Phone:</div>
									<div class="grid_8 contactFieldValue">(123) 123-4567</div>
									<div class="clear"></div>
									<div class="grid_2 contactFieldDesc">Fax:</div>
									<div class="grid_8 contactFieldValue">(123) 123-4567</div>
									<div class="clear"></div>
									<div class="grid_2 contactFieldDesc">Toll Free:</div>
									<div class="grid_8 contactFieldValue">(800) 123-4567</div>
									<div class="clear"></div>
								</div>
								<div class="clear"></div>

								<div id="contact-wrapper">
<?php								    // Message Area.  It shows a message upon successful email submission
								    if( isset( $emailSent ) && $emailSent == true ) : ?>
									<div class="success">
									    <strong>Email Successfully Sent!</strong><br />
									    Thank you <strong><?php echo $contact_name_thx; ?></strong> for using our contact form! Your email was successfully sent and we will be in touch with you shortly.
									</div>
<?php								    elseif ( isset( $emailSent ) && $emailSent == false ) : ?>
									<div class="erroneous">
									    Failed to connect to mailserver!
									</div>
<?php								    endif; ?>

								    <form id="contactForm" class="cmxform" method="post" action="#contact-wrapper">
									<strong>Please use the form below to send us an email:</strong>
									<div>
									    <label for="contact_name">Name </label><em>(required, at least 2 characters)</em><br />
									    <input id="contact_name" name="contact_name" size="30" class="required<?php if(isset($nameError)) echo ' error'; ?>" minlength="2" value="<?php echo htmlentities($contact_name, ENT_QUOTES, "UTF-8"); ?>" />
<?php									    if(isset($nameError)) echo '<label class="error" for="contact_name" generated="true">'.$nameError.'</label>'; ?>
									</div>
									<div>
									    <label for="contact_email">E-Mail </label><em>(required)</em><br />
									    <input id="contact_email" name="contact_email" size="30"  class="required email<?php if(isset($emailError)) echo ' error'; ?>" value="<?php echo htmlentities($contact_email, ENT_QUOTES, "UTF-8"); ?>" />
<?php									    if(isset($emailError)) echo '<label class="error" for="contact_email" generated="true">'.$emailError.'</label>'; ?>
									</div>
									<div>
									    <label for="contact_phone">Phone </label><em>(optional)</em><br />
									    <input id="contact_phone<?php echo $NA_phone_format; ?>" name="contact_phone<?php echo $NA_phone_format; ?>" size="14" class="phone<?php if(isset($phoneError)) echo ' error'; ?>" value="<?php echo htmlentities($contact_phone, ENT_QUOTES, "UTF-8"); ?>" maxlength="14" />
									    <label for="contact_ext">ext. </label>
									    <input id="contact_ext<?php echo $NA_phone_format; ?>" name="contact_ext<?php echo $NA_phone_format; ?>" size="5" class="ext<?php if(isset($extError)) echo ' error'; ?>" value="<?php echo htmlentities($contact_ext, ENT_QUOTES, "UTF-8"); ?>" maxlength="5" />
<?php									    if(isset($phone_extError)) echo '<label class="error" for="contact_phone" generated="true">'.$phone_extError.'</label>'; ?>
									</div>
									<div>
									    <label for="contact_message">Your comment </label><em>(required)</em><br />
									    <textarea id="contact_message" name="contact_message" cols="70" rows="7" class="required<?php if(isset($messageError)) echo ' error'; ?>"><?php echo htmlentities($contact_message, ENT_QUOTES, "UTF-8"); ?></textarea>
<?php								    	    if(isset($messageError)) echo '<br /><label class="error" for="contact_message" generated="true">'.$messageError.'</label>'; ?>
									</div>
									<div>
									    <input name="submit" class="submit" type="submit" value="Submit"/>
									</div>
								    </form>

								</div><!-- end contact-wrapper -->
							</div>
							<!-- end page_content -->
							<div id="sidebar" class="grid_4 pull_11">
								<div id="sidebarSubnav">
									<div>
										<h3>Business Hours:</h3>
										<div class="textwidget">
											<div style="float:left;width:120px">
												<strong>Monday:</strong><br />
												<strong>Tuesday:</strong><br />
												<strong>Wednesday:</strong><br />
												<strong>Thursday:</strong><br />
												<strong>Friday:</strong><br />
												<strong>Saturday:</strong><br />
												<strong>Sunday:</strong>
											</div>
											<div style="float:left;text-align:right">
												<em>9am</em> to <em>5pm</em><br />
												<em>9am</em> to <em>5pm</em><br />
												<em>9am</em> to <em>5pm</em><br />
												<em>9am</em> to <em>5pm</em><br />
												<em>9am</em> to <em>5pm</em><br />
												<em>10am</em> to <em>2pm</em><br />
												<em>Closed</em>
											</div>
											<div class="clear"></div>
										</div>
									</div>
									<div class="loginform">
										<h3>Login Form</h3>
										<ul>
											<div>
												<form action="#" method="post">
													<p>
														<label for="log">User<br />
															<input type="text" name="log" id="log" value="" size="20" />
														</label><br />
														<label for="pwd">Password<br />
															<input type="password" name="pwd" id="pwd" size="20" />
														</label>
														<div>
															<input type="submit" name="submit" value="Login" class="button" />
															<label for="rememberme"><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" /> Remember me</label>
														</div>
													</p>
													<input type="hidden" name="redirect_to" value="#" />
												</form>
											</div>
											<li><a href="#">Recover password</a></li>
										</ul>
									</div>
									<div class="widget_googlemap">
										<h3>Location Map</h3>
										<iframe width="220" height="300" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.ca/maps?f=q&source=s_q&hl=en&geocode=&q=New+York&sll=49.891235,-97.15369&sspn=47.259509,86.923828&ie=UTF8&hq=&hnear=New+York,+United+States&ll=40.714867,-74.005537&spn=0.019517,0.018797&z=14&iwloc=near&output=embed">
										</iframe>
										<br />
										<small><a href="http://maps.google.ca/maps?f=q&source=embed&hl=en&geocode=&q=New+York&sll=49.891235,-97.15369&sspn=47.259509,86.923828&ie=UTF8&hq=&hnear=New+York,+United+States&ll=40.714867,-74.005537&spn=0.019517,0.018797&z=14&iwloc=near" style="color:#0000FF;text-align:left">View Larger Map</a></small>
									</div>
								</div>
							</div>
							<!-- end sidebar -->
						</div>
						<!-- end container_bkgnd_btm -->
					</div>
					<!-- end page_container -->
					<div class="clear"></div>
					<div id="bottom_lid" class="grid_16 pngfix"> </div>
					<div class="clear"></div>
					<div id="bottom_widgets" class="grid_16">
						<div id="bottom_wrapper" class="bottom_wrapper">
							<div id="bottom_1" class="column_1_of_4">
								<div class="bottom_1 widget_latest_posts">
									<div class="bottom_heading_m">
										<div class="bottom_heading_l">
											<div class="bottom_heading_r">
												<h3 class="bottom_1_title">Latest Posts</h3>
											</div>
										</div>
									</div>
									<div id="widget_latest_posts">
										<div class="latest_posts">
											<h4><a href="#">Blog Post 5</a></h4>
											<small>January 6th, 2010 by <a href="#" title="Posts by admin">admin</a></small>
											<p>This is a test…Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean
												commodo ligula eget dolor. Aenean massa. <small class="read_more"><a href="#"> (more…)</a></small></p>
										</div>
										<!-- end widget_recent_posts -->
									</div>
									<!-- end widget_recent_posts -->
								</div>
							</div>
							<!-- end bottom_1 -->
							<div id="bottom_2" class="column_1_of_4">
								<div class="bottom_2 widget_text">
									<div class="bottom_heading_m">
										<div class="bottom_heading_l">
											<div class="bottom_heading_r">
												<h3 class="bottom_2_title">Disclaimer</h3>
											</div>
										</div>
									</div>
									<div class="textwidget"><strong>Important</strong>: This demo is purely for demonstration purposes and all the content are
										designed to showcase the <strong><em>Business Success</em></strong> WP theme on a live site. All images are copyrighted to their respective owners.</div>
								</div>
							</div>
							<!-- end bottom_2 -->
							<div id="bottom_3" class="column_1_of_4">
								<div class="bottom_3 widget_recent_comments">
									<div class="bottom_heading_m">
										<div class="bottom_heading_l">
											<div class="bottom_heading_r">
												<h3 class="bottom_3_title">Recent Comments</h3>
											</div>
										</div>
									</div>
									<ul id="recentcomments">
										<li class="recentcomments">admin on <a href="single.html#comment-46">Blog Post 5</a></li>
										<li class="recentcomments">admin on <a href="single.html#comment-45">Blog Post 5</a></li>
										<li class="recentcomments">admin on <a href="single.html#comment-43">Blog Post 5</a></li>
									</ul>
								</div>
							</div>
							<!-- end bottom_3 -->
							<div id="bottom_4" class="column_1_of_4">
								<div class="bottom_4 widget_text">
									<div class="bottom_heading_m">
										<div class="bottom_heading_l">
											<div class="bottom_heading_r">
												<h3 class="bottom_4_title">Heading Title</h3>
											</div>
										</div>
									</div>
									<div class="textwidget">Aoccdrnig to rscheearch at an Elingsh uinervtisy, it deosn't mttaer in waht
										oredr the ltteers in a wrod are, olny taht the frist and lsat ltteres are at the
										rghit pcleas. The rset can be a toatl mses and you can sitll raed it.</div>
								</div>
							</div>
							<!-- end bottom_4 -->
						</div>
						<!-- end bottom_wrapper -->
					</div>
					<!-- end bottom_widgets -->
					<div class="clear"></div>
					<div id="footer_lid" class="grid_16 pngfix"> </div>
					<div class="clear"></div>
					<div id="footer" class="grid_16 pngfix">
						<div id="footer_text" class="grid_9 alpha">
							<p>© 2010 <strong>Business Success</strong> | <a href="#">Entries (RSS)</a> | <a href="#">Comments (RSS)</a></p>
						</div>
						<div id="footer_social" class="grid_7 omega">
							<ul>
								<li class="social_icon"><a href="http://www.facebook.com/sharer.php?u=http://this-site-url" title="Facebook" target="_blank"><img src="styles/common-images/facebook_icon.png" alt="facebook" border="0" /></a></li>
								<li class="social_icon"><a href="http://del.icio.us/post?url=http://this-site-url" title="Delicious" target="_blank"><img src="styles/common-images/delicious_icon.png" alt="delicious" border="0" /></a></li>
								<li class="social_icon"><a href="http://digg.com/submit?url=http://this-site-url" title="Digg" target="_blank"><img src="styles/common-images/digg_icon.png" alt="digg" border="0" /></a></li>
								<li class="social_icon"><a href="#" title="RSS" target="_blank"><img src="styles/common-images/rss_icon.png" alt="rss" border="0" /></a></li>
								<li class="social_icon"><a href="http://twitter.com/home?status=Business Success%20-%20http://this-site-url" title="Twitter" target="_blank"><img src="styles/common-images/twitter_icon.png" alt="twitter" border="0" /></a></li>
								<li class="social_icon"><a href="contact.php#contact-wrapper" title="E-mail"><img src="styles/common-images/email_icon.png" alt="email" border="0" /></a></li>
							</ul>
						</div>
					</div>
					<div class="clear"></div>
					<script type="text/javascript" src="scripts/prettyPhoto/custom_params.js?ver=1.0"></script>
				</div>
				<!-- end page -->
			</div>
			<!-- end wrapper-2 -->
		</div>
		<!-- end wrapper-1 -->
		<script type="text/javascript"> Cufon.now(); </script>
	</body>
</html>

<?php

/**
 * Check the validity of the given Phone Numbers (North American)
 * This regex will validate a 10-digit North American telephone number.
 * Separators are not required, but can include spaces, hyphens, or periods.
 * Parentheses around the area code are also optional.
 *
 * @param string $phone The phone number
 * @return bool true if the phone number is valid or false otherwise
 */
function isPhoneNumberValid( $phone ) {
    // validate a phone number
    $pattern = '/^((([0-9]{1})*[- .(]*([0-9]{3})[- .)]*[0-9]{3}[- .]*[0-9]{4})+)*$/';
    return preg_match( $pattern, $phone );
}

/**
 * Validate E-mail address with Regular Expression.
 * Also it validates the domain with the 'getmxrr(..)' function
 *
 * @param string $email The E-mail to be checked
 *
 * @return boulean $valid Reterns 'true' of 'false'
 */
function valid_email( $email ){
    // Create the syntactical validation regular expression
    $pattern = "/^([_a-z0-9-]+)(\.[_a-z0-9-]+)*@([a-z0-9-]+)(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/";
    // Presume that the email is invalid
    $valid = 0;
    // Validate the syntax
    if ( preg_match( $pattern, $email ) ){
        list( $username, $domaintld ) = split( "@", $email );
	$valid = 1;
    } else {
        $valid = 0;
    }
    return $valid;
}

/**
 * Send PHP mail with UTF-8 support
 *
 * @param string $to Receiver, or receivers of the mail
 * @param string $subject Subject of the email to be sent
 * @param string $message Message to be sent
 * @param string $header String to be inserted at the end of the email header
 *
 * @return boulean $mail Returns TRUE if the mail was successfully accepted for delivery, FALSE otherwise
 */
function mail_utf8($to, $subject = '(No subject)', $message = '', $header = '') {
    $header_ = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/plain; charset=UTF-8' . "\r\n";
    return @mail($to, '=?UTF-8?B?'.base64_encode($subject).'?=', $message, $header_ . $header);
}

