<?php 

class CustomManagementUsersTestManagerController extends modExtraManagerController {

	public function initialize(){
		parent::initialize();
	}

	public function getPageTitle(){
		return 'FLORINDO LOPEZ DELGADO';
	}

	public function getTemplateFile(){

		$this->setPlaceholder('data', array('name' => 'FLORINDO LOPEZ DELGADO'));

		return 'management/users/test.tpl';
	}
}