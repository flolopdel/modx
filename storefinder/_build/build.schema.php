<?php
/**
 * Build Schema script
 *
 * @package storefinder
 * @subpackage build
 */
require_once dirname(__FILE__) . '/build.config.php';
include_once MODX_CORE_PATH . 'model/modx/modx.class.php';
$modx= new modX();
$modx->initialize('mgr');
$modx->loadClass('transport.modPackageBuilder','',false, true);
$modx->setLogLevel(modX::LOG_LEVEL_INFO);
$modx->setLogTarget(XPDO_CLI_MODE ? 'ECHO' : 'HTML');
$root = dirname(dirname(__FILE__)).'/';
$sources = array(
    'root' => $root,
    'core' => $root.'core/components/storefinder/',
    'model' => $root.'core/components/storefinder/model/',
    'assets' => $root.'assets/components/storefinder/',
    'schema' => $root.'_build/schema/',
);

// 2.------------------------------
$manager= $modx->getManager();
$generator= $manager->getGenerator();

// 3.------------------------------
$generator->parseSchema($sources['schema'].'storefinder.mysql.schema.xml', $sources['model']);
$modx->addPackage('storefinder', $sources['model']); // add package to make all models available
$manager->createObjectContainer('Storefinder'); // created the database table
$modx->log(modX::LOG_LEVEL_INFO, 'Done!');


$mtime= microtime();
$mtime= explode(" ", $mtime);
$mtime= $mtime[1] + $mtime[0];
$tend= $mtime;
$totalTime= ($tend - $tstart);
$totalTime= sprintf("%2.4f s", $totalTime);
echo "\nExecution time: {$totalTime}\n";
exit ();